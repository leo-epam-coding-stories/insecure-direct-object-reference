package com.epam.database.service;

import java.util.List;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.database.model.Customer;

@Service
public class CustomerServiceImpl implements CustomerService{
	
	private final JdbcTemplate jdbcTemplate;
	
	public CustomerServiceImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public List<Customer> getAllCustomers() {
        String sql = "SELECT customerId, customerName, status FROM CUSTOMER";
        return this.jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Customer.class));
	}

	
	@ExceptionHandler(EmptyResultDataAccessException.class)
    public ResponseEntity<String> noCityFound(EmptyResultDataAccessException e) {

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No data found.");
    }
}
