package com.epam.database.service;

import java.util.List;

import com.epam.database.model.Customer;

public interface CustomerService {

	List<Customer> getAllCustomers();
	
}
