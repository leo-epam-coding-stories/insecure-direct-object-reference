package com.epam.database.model;

/**
 * @author Vijay_Leo_Marcelin
 *
 */
public class Customer {

	private String customerName;
	private String status;
	private String customerId;
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) throws Exception {
		this.customerId = customerId;
	}
}
