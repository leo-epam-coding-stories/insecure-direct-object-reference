package com.epam.frontend;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.database.model.Customer;
import com.epam.database.service.CustomerService;

@RestController
public class CustomerDetailsController {
	
	Logger log = Logger.getLogger(this.getClass().getName());
	
	private CustomerService customerService;
	
	public CustomerDetailsController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@RequestMapping(value = "/getAllCustomers")
	public ResponseEntity<Object> getCustomers() {
		List<Customer> customerList = null;
		try {
			customerList = customerService.getAllCustomers();		
		}catch(Exception ex) {
			return new ResponseEntity<>("Exception Occurred in the process:"+ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(customerList, HttpStatus.OK);
	}
}
