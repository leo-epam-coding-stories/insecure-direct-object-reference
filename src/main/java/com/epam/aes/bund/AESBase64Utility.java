package com.epam.aes.bund;

import static com.epam.aes.bund.AESBase64UtilityConstant.AES_ALGO_TXT;
import static com.epam.aes.bund.AESBase64UtilityConstant.ENCRYPT_ALGO;
import static com.epam.aes.bund.AESBase64UtilityConstant.IV_LENGTH_BYTE;
import static com.epam.aes.bund.AESBase64UtilityConstant.SALT_LENGTH_BYTE;
import static com.epam.aes.bund.AESBase64UtilityConstant.TAG_LENGTH_BIT;
import static com.epam.aes.bund.AESBase64UtilityConstant.getAESKeyFromPassword;
import static com.epam.aes.bund.AESBase64UtilityConstant.getRandomNonce;

import java.nio.ByteBuffer;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author Vijay_Leo_Marcelin
 *
 */
public class AESBase64Utility {
		
	/**
	 * @param pText
	 * @param password
	 * @return
	 * @throws Exception
	 */
	public static String encrypt(byte[] pText) throws Exception {

		byte[] salt = getRandomNonce(SALT_LENGTH_BYTE);

		byte[] iv = getRandomNonce(IV_LENGTH_BYTE);

		// Get the secret key from the ALGO TXT constant
		SecretKey aesKeyFromPassword = getAESKeyFromPassword(AES_ALGO_TXT.toCharArray(), salt);

		Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

		cipher.init(Cipher.ENCRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

		byte[] cipherText = cipher.doFinal(pText);

		// Prefix IV and Salt to cipher text
		byte[] cipherTextWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherText.length).put(iv).put(salt)
				.put(cipherText).array();

		return Base64.getEncoder().encodeToString(cipherTextWithIvSalt);

	}
	
	/**
	 * This method is unused and just for reference to the user on re-confirmation
	 * in test method to identify the actual value(Real Value). Since we have used
	 * AES(Advanced Encryption Standards), the initialisation vector used in CBC mode 
	 * is a random block, so each encryption will be different. An IV is sort of like 
	 * a salt, except when encrypting a message there is no reason to ever specify any 
	 * specific IV to use (a random IV should always be used). The IV is often put in 
	 * front of the first block of the encrypted message.
	 * 
	 * @param cText
	 * @param password
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	public static String decrypt(String cText) throws Exception {

		byte[] decode = Base64.getDecoder().decode(cText.getBytes(UTF_8));

		// Get the iv and salt from the cipher text
		ByteBuffer bb = ByteBuffer.wrap(decode);

		byte[] iv = new byte[IV_LENGTH_BYTE];
		bb.get(iv);

		byte[] salt = new byte[SALT_LENGTH_BYTE];
		bb.get(salt);

		byte[] cipherText = new byte[bb.remaining()];
		bb.get(cipherText);

		// Get  the AES key from the same AES ALGO TXT and salt
		SecretKey aesKeyFromPassword = getAESKeyFromPassword(AES_ALGO_TXT.toCharArray(), salt);

		Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

		cipher.init(Cipher.DECRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

		byte[] plainText = cipher.doFinal(cipherText);

		return new String(plainText, UTF_8);

	}
}
