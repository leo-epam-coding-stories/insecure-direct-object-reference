package com.epam.aes.bund;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author Vijay_Leo_Marcelin
 *
 */
public class AESBase64UtilityConstant {

	public static final String AES_INSTANCE_TYPE = "PBKDF2WithHmacSHA256";

	public static final String ENCRYPT_ALGO = "AES/GCM/NoPadding";

	public static final String AES_ALGO_TXT = "EPAM";

	public static final int TAG_LENGTH_BIT = 112;

	public static final int IV_LENGTH_BYTE = 12;

	public static final int SALT_LENGTH_BYTE = 8;

	public static final Charset UTF_8 = StandardCharsets.UTF_8;

	/**
	 * @param bytes
	 * @return
	 */
	public static byte[] getRandomNonce(int bytes) {
		byte[] nonce = new byte[bytes];
		new SecureRandom().nextBytes(nonce);
		return nonce;
	}

	/**
	 * @param password
	 * @param salt
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	public static SecretKey getAESKeyFromPassword(char[] password, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {

		SecretKeyFactory factory = SecretKeyFactory.getInstance(AES_INSTANCE_TYPE);
		int iterationCount = 65536;
	    int keyLength = 256;
		KeySpec spec = new PBEKeySpec(password, salt, iterationCount, keyLength);
		SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
		return secret;

	}

}
